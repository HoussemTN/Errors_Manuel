import 'dart:convert';
import 'package:errors_manual/libraries/Private.dart' as private;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'libraries/globals.dart' as globals;
import 'ComboBoxProductBrand.dart';
import 'model/Error.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity/connectivity.dart';
import 'AddError.dart';
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  final _codeController = TextEditingController();
  // Searched Code
  String _code;
  String _description;
  bool _isSearching = false;
 Future<Map<String,String>> errors ;
///TODO : search in cached files (SharedPreferences)
   Map<String,String> convertMapKeysToLowerKeys(Map<String,String> mapToConvert){
      Map<String,String> convertedMap= Map();
      for(String key in mapToConvert.keys){
        convertedMap[key.toLowerCase()]=mapToConvert[key];

      }
   return convertedMap;
  }

  static _reviver(key, value) {
    /// last node is null , our value is map , the key of map starts with "-"
    if(key!=null && value is Map && key.contains("-")){
      return new Error.fromJson(value);
    }
    return value;
  }
    var jsonCodec = JsonCodec(reviver:_reviver);


  /// ---[FUNCTION] loads Codes from firebase and add them to prefs --- ///
   _loadRecords () async {
     var connectivityResult = await (Connectivity().checkConnectivity());
     // Connected to mobile or Wifi
     if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
     SharedPreferences prefs = await SharedPreferences.getInstance();
     prefs.clear();
    var url=private.url+"codes.json";
    final httpClient = new Client();
    var response = await httpClient.read(url);
    /// Result nested map
    Map<String,dynamic> result=json.decode(response);
    /// Convert map values to list of map (code:"",Description:"")
    List finalResult=result.values.toList();
    for(int i=0;i<finalResult.length;i++){
      await prefs.setString(finalResult[i]["Code"],finalResult[i]["Description"]);
    }
   //print(prefs.getKeys().toString());
     }
  }
  /// ---[FUNCTION] Search Codes --- ///
  void searchEngine(String _codeImploded) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    /// if Code[String] exists (Sensitive Case)
    if (prefs.containsKey(_codeImploded)) {
      _code = _codeController.text;
      _description = prefs.get(_codeImploded);

      /// if LowerCase Table[Map] is Ready Check Code
    } else if (globals.lowerCaseCodes.containsKey(
        _codeImploded.toLowerCase())) {
      _code = _codeController.text;
      _description =
      globals.lowerCaseCodes[_codeImploded
          .toLowerCase()];
    }

    /// convert codes[Map] to lower case (Ignoring Case)
    else if (globals.lowerCaseCodes.isEmpty) {
     /// Get List of keys (Current Case)
      List toLowerCaseList =prefs.getKeys().toList();
      /// Convert keys to lower Case
      for(int i=0;i<toLowerCaseList.length;i++){
        globals.lowerCaseCodes.addAll({toLowerCaseList[i].toString().toLowerCase():prefs.get(toLowerCaseList[i].toString())});
      }
      if (globals.lowerCaseCodes.containsKey(_codeImploded.toLowerCase())) {
        _code = _codeController.text;
        _description = globals.lowerCaseCodes[_codeImploded.toLowerCase()];
      }
    } else {
      _code = null;
      _description = null;
    }
  }

  @override
  void initState() {

   _loadRecords();
    super.initState();
  }
    @override
    Widget build(BuildContext context) {
      Widget renderResult(String code, String description) {
        ///Code found => return _code[String] and _description[String]
        if (code != null) {
          return Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    decoration:BoxDecoration(
                        color:Colors.green,
                      borderRadius: BorderRadius.all(Radius.circular(40))
                    ),
                    margin:EdgeInsets.only(right:4.0,bottom: 8.0),
                    child:  IconButton(
                      icon: Icon(Icons.edit,color: Colors.white,),
                      onPressed: null,
                    ),
                  ),
                  Container(
                    decoration:BoxDecoration(
                        color:Colors.red,
                        borderRadius: BorderRadius.all(Radius.circular(40))
                    ),
                    margin: EdgeInsets.only(right:12.0,bottom: 8.0),
                    child:  IconButton(
                      icon: Icon(Icons.delete,color: Colors.white,),
                      onPressed: null,
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.94,
                color: Colors.grey[300],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Code : $code",
                      style: TextStyle(fontWeight: FontWeight.w800, fontSize: 19.0),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 8.0),
                    ),
                    Text(
                      "$description ",
                      style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.0),
                    ),
                  ],
                ),
              ),
            ],
          );
        }
        // Code Not Found Return a message
        else {
          return Container(
            padding: EdgeInsets.all(8.0),
            width: MediaQuery
                .of(context)
                .size
                .width * 0.94,
            color: Colors.red[300],
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Code introuvable !",
                  style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 19.0,
                      color: Colors.white),
                ),
              ],
            ),
          );
        }
      }

      return MaterialApp(
          home:Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView(
          children: <Widget>[
            //Root Container
            Container(
              child: Column(
                children: <Widget>[
                  // Page Padding
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  Row(
                    children: <Widget>[
                      SizedBox(width: 10.0,),
                      Expanded(
                        child: Form(
                          key: _formKey,
                          child: Column(

                            children: <Widget>[
                              Center(
                                child: Container(

                                  child: TextFormField(
                                    controller: _codeController,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: "Code",
                                      hintText: "Code",
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Code Obligatoire';
                                      }
                                      return null;
                                    },
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ),
                      ),

                      Flexible(
                          child: ComboBoxProductBrand()),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(4.0),
                  ),
                  Container(
                    height: 40,
                    child: RaisedButton.icon(
                        label: Text('Rechercher'),
                        icon: Icon(Icons.search),
                        onPressed: () {
                          setState(() {
                            if (_formKey.currentState.validate() == true) {
                              _isSearching = true;
                              var _codeImploded = _codeController.text + '-' + globals.currentBrand;
                              searchEngine(_codeImploded);
                            }
                          });
                        }),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8.0),
                  ),
                  Container(
                    child: Container(
                      child: _isSearching
                          ? renderResult(_code, _description)
                          : Container(
                          child:
                          Text("Entrez un code Pour connaître la cause")),
                    ),
                  ),

                ],
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
           Navigator.push(context,MaterialPageRoute(builder:(context)=>AddError(),),);
          },
          child: Icon(Icons.add),
          backgroundColor: Colors.blue,
        ),
          ),
      );
    }

}
