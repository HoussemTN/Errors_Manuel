import 'package:flutter/material.dart';
import 'libraries/globals.dart' as globals;
class ComboBoxProductBrand extends StatefulWidget {
  ComboBoxProductBrand({Key key}) : super(key: key);

  @override
  _ComboBoxProductBrandState createState() => new _ComboBoxProductBrandState();
}

class _ComboBoxProductBrandState extends State<ComboBoxProductBrand> {
  List _brands = ["SUMSUNG", "LG", "ARISTON", "Haier", "ECOLUX", "Midea","Saba","gree S","Beko S","Beko","Galanz S","TCL S","Whirlpool","Electrolux"];

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _currentBrand;

  @override
  void initState() {
    _dropDownMenuItems = getDropDownMenuItems();
    _currentBrand = _dropDownMenuItems[0].value;
    globals.currentBrand=_currentBrand;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = new List();
    for (String brand in _brands) {
      items.add(new DropdownMenuItem(value: brand, child: new Text(brand)));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      // padding of ComboBox
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: Container(
          padding: EdgeInsets.all(4.0),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey,
            ),
            borderRadius: BorderRadius.circular(4.0),
          ),
          child: Container(
            margin: EdgeInsets.only(right: 8.0, left: 8.0),
            child: DropdownButtonHideUnderline(
              child: new DropdownButton(
                isExpanded: true,
                iconSize: 25.0,
                value: _currentBrand,
                items: _dropDownMenuItems,
                onChanged: changedDropDownItem,
                style: Theme.of(context).textTheme.title,
              ),
            ),
          ),
        ),
      ),
    );
  }

  void changedDropDownItem(String selectedCity) {
    setState(() {
      _currentBrand = selectedCity;
      globals.currentBrand=_currentBrand;
    });
  }
}
