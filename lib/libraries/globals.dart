library errors_manual.globals;
String currentBrand ;
Map<String,String> lowerCaseCodes=Map();
/*Map<String,String> codes= {
  /// -------SUMSUNG-------
  // PAGE  1
  "1E-SUMSUNG":"- Vérifier la chambre d'air \n - Vérifier la durite de pressostat \n - Vérifier les connections de pressostat 2.5V DC \n - Remplacer le pressostat \n - Remplacer la carte",
  "3E-SUMSUNG":"- Remplacer le sensor \n - Remplacer le stator \n - Remplacer la carte",
  "3E1-SUMSUNG":"- Remplacer le sensor \n - Remplacer le stator \n - Remplacer la carte",
  "3E2-SUMSUNG":"- Remplacer le sensor \n - Remplacer le stator \n - Remplacer la carte",
  "3E3-SUMSUNG":"- Remplacer le sensor \n - Remplacer le stator \n - Remplacer la carte",
  "3E4-SUMSUNG":"- Remplacer le sensor \n - Remplacer le stator \n - Remplacer la carte",
  "4E-SUMSUNG":"- Vérifier le robinet et la pression d'eau \n - Véfifier le tuyau de remplissage \n - Vérifier les connections de l'électrovanne \n - Remplacer la carte",
  "4E1-SUMSUNG":"- Vérifier le robinet et la pression d'eau \n - Véfifier le tuyau de remplissage \n - Vérifier les connections de l'électrovanne \n - Remplacer la carte",
  "4E2-SUMSUNG":"- Vérifier le robinet et la pression d'eau \n - Véfifier le tuyau de remplissage \n - Vérifier les connections de l'électrovanne \n - Remplacer la carte",
  "5E-SUMSUNG":"- Vérifier le moteur de vidange \n - Vérifier la pompe de vidange \n - Remplacer la carte",
  "AE-SUMSUNG":"Problème de communication entre carte et afficheur : \n - Vérifier les connexions entre carte et afficheur",
  "bE1-SUMSUNG":"Problème de Switch tactiles au niveau de display : \n - Vérifier la position du contrôle panel \n - Vérifier les Switch tactiles \n - Remplacer la carte",
  "bE2-SUMSUNG":"Problème de Switch tactiles au niveau de display : \n - Vérifier la position du contrôle panel \n - Vérifier les Switch tactiles \n - Remplacer la carte",
  "bE3-SUMSUNG":"Problème de Switch tactiles au niveau de display : \n - Vérifier la position du contrôle panel \n - Vérifier les Switch tactiles \n - Remplacer la carte",
  "CE-SUMSUNG":"Problème sonde de température : \n - Vérifier câblage \n - Remplacer la sonde de température \n - Remplacer la résistance de lavage \n - Remplacer la carte",
  "dE-SUMSUNG":"Problème verrouillage porte \n Problème pompe de vidange \n Problème de moteur de séchage \n - Vérifier la valeur de résistance entre les deux cosses 1 et 3 et 949 Ω",
  "dE1-SUMSUNG":"Problème verrouillage porte \n Problème pompe de vidange \n Problème de moteur de séchage \n - Vérifier la valeur de résistance entre les deux cosses 1 et 3 et 949 Ω",
  "dE2-SUMSUNG":"Problème verrouillage porte \n Problème pompe de vidange \n Problème de moteur de séchage \n - Vérifier la valeur de résistance entre les deux cosses 1 et 3 et 949 Ω",
  // PAGE 3
  "FE-SUMSUNG":"Problème de moteur de séchage : \n - Vérifier le câblage et la fiche de contact \n - Vérifier le niveau de tension au moteur lors de séchage 230 V et résistance 298 Ω",
  "HE-SUMSUNG":"Problème de résistance",
  "HE1-SUMSUNG":"Problème de résistance",
  "HE2-SUMSUNG":"Problème de résistance",
  "HE3-SUMSUNG":"Problème de résistance",
  "LE-SUMSUNG":"Problème fuite d'eau : \n - Vérifier les fuites d'eau au niveau des bellows de la machine à laver ",
  "LE1-SUMSUNG":"Problème fuite d'eau : \n - Vérifier les fuites d'eau au niveau des bellows de la machine à laver ",
  "OE-SUMSUNG":"Problème excés de remplissage en eau : \n - Vérifier la durite de pressostat \n - Vérifier l'électrovanne \n - Remplacer la carte",
  "OF-SUMSUNG":"Problème excés de remplissage en eau : \n - Vérifier la durite de pressostat \n - Vérifier l'électrovanne \n - Remplacer la carte",
  "tE1-SUMSUNG":"Problème sonde de séchage : \n - Vérifier la sonde de lavage \n - Vérifier la sonde de séchage \n - Remplacer la carte",
  "tE2-SUMSUNG":"Problème sonde de séchage : \n - Vérifier la sonde de lavage \n - Vérifier la sonde de séchage \n - Remplacer la carte",
  "tE3-SUMSUNG":"Problème sonde de séchage : \n - Vérifier la sonde de lavage \n - Vérifier la sonde de séchage \n - Remplacer la carte",
  "UE-SUMSUNG":"Problème de déséquilibrage : \n - Vérifier que la produit est installé sur une surface plate \n - Vérifier que la linge est bien posé dans la cuve \n - Vérifier le sensor \n - Remplacer la carte",
  /// -------LG-------
  "IE-LG":" - Filtre de vanne d'arrivée d'eau est bloqué  \n - Mauvaise connexion du tuyau d'eau \n - L'inlet valve es défecetueux \n - Le pressostat est défectueux \n - Carte mére défectueuse \n",
  "OE-LG":"- Blocage par un corps étrangé dans le belloxs \n - Blocage par un corps étrangé dans la pompe de vidange \n - Pompe de vidange est défectueuse \n - Drain Hose Bloqué \n - Vérifier les connecteurs",
  "dE-LG":"- Door Latch transformé \n - Door Switch transformé \n - Défaut d'assembalge de Door Switch \n - Door Switch defectueux",
  "LE-LG":"Erreur de rotation de moteur : \n Bobinage de moteur déconnectés \n - Hall Sensor defectueux \n - Carte defectueuse",
  "CE-LG":"Surintensité lors de fonctionnement : \n - Bobinage de stator en court circuit(résistance 7~10Ω) \n - Vérifier Tension 12V au bornes du hall sensor",
  "dHE-LG":"Ne sèche pas bien : \n - Fan de ventilateur bloqué \n - Thermostat déconnecté \n - Heater déconnecté \n - Thermostat au niveau de duct déconnecté \n - Corps étranger dans le ventilateur de duct",
  "tE-LG":"Thermistor déconnecté \n - Connecteur débranché \n - Thermistor déconnecté \n - Heater déconnecté",
  "FE-LG":"Excés de replissage en eau : \n - Inlet valve défectueux \n - Mauvaise utilisation \n - Carte défectueuse",
  "UE-LG":"Diséquilibre pendant l'essorage : \n - Démonter les vis de fixation \n - Machine intallé sur plafond incliné \n - Charge incliné à un seul coté \n - Amortisseur",
  /// -------ARISTON-------
  "F01-ARISTON":"- Carte mére,Triac de la carte abîmé \n - Feedback composants en panne",
  "F02-ARISTON":"- Moteur bloqué,compte-tours moteur \n - Verte n court-circuit",
  "F03-ARISTON":"- Sonde NTC lavage ouverte \n - Court-circuit",
  "F05-ARISTON":"- Vide pressostat non atteint \n - Pompe de vidiange bloquée",
  "F06-ARISTON":"- Verrouillage de la porte ne se ferme/ouvre pas \n -Triac verrouillage de la porte PTC overt/fermé \n - Signal fréquence de réseau en panne \n - Signal tension de réseau en panne",
  "F07-ARISTON":"- Relais de la résistance de lavage ouvert \n - Relais de l'iverseur collé côté pompe de vidange",
  "F08-ARISTON":"- Relais de résistance de lavage par dispersion / résistance liée",
  "F09-ARISTON":"- Erreur de fichier de paramétrage affichée dans l'onglet principal",
  "F11-ARISTON":"- Pompes débranchées \n  - Triac pilote des pompes en cout-circuit",
  "F12-ARISTON":"- Erreur de communication entre carte mére et carte d'affichage",
  "F13-ARISTON":"- Sonde NTC de séchage en court-circuit \n - Sonde NTC de séchage ouvert",
  "F15-ARISTON":"- Résistance du séchoir en dispersion ou court-circuit \n - Relais de la carte collé",
  "F18-ARISTON":"- Abscence de communication UART entre DSP et la carte mére",
  /// -------Haier-------
  "Err1-Haier":"Problème porte ne se ferme pas : \n - Vérifier la bonne fermeture de porte \n - Vérifier le câblage au niveau de switch \n - Mesurer la tension au niveau de switch =220V AC \n - Remplacer le switch \n - Remplcaer la carte",
  "Err2-Haier":"Problème de vidange : \n - Vérifier que le tuyau de drainage est bien positionné \n - Vérifier si la pompe de vidange est bloquée \n - Vérifier le câblage et la tension au niveau de la pompe de vidange \n - Remplace la pompe de vidange \n - Remplacer la carte",
  "Err3-Haier":"Problème sonde de température : \n - Mesurer la résistance de thermistor entre 5 et 14 KΩ \n - Vérifier le câblage et la fixation de la sonde \n - Remplacer le thermistor \n -Remplacer la carte",
  "Err4-Haier":"Probléme résistance de lavage : \n - Vérifier le câblage \n - Mesurer la résistance de lavage entre 32 et 35Ω \n - Remplacer la résistance de lavage \n - Remplacer la carte",
  "Err5-Haier":"Problème pas de remplissage en eau : \n - Vérifier que le robinet est ouvert \n - Vérifier que le noyeau de remplissage n'est pas écrasé \n - Vérifier que le tuyau de vidange n'est pas tombé \n - Vérifier les filtres de l'électrovanne \n - Vérifier le câblage et la tension aux bornes de l'électrovanne \n - Remplacer l'électrovanne \n - Remplacer la carte",
  "Err7-Haier":"Problème moteur : \n - Vérifier le câblage de moteur \n - Mesusrer la résistance entre blanc et blanc 5Ω \n - Mesurer la résistance entre marron et rouge 4Ω \n - Mesurer la résistance entre gris et noir 40Ω \n - Remplacer le moteur \n - Remplacer la carte",
  "Err8-Haier":"Problème excés de remplissage en eau : \n - Vérifier la chambre d'air et la durite de pressostat \n - Remplacer l'électrovanne si même hors tension il y a une entrée d'eau \n - Vérifier le câblage de pressostat \n - Remplacer la carte",
  /// -------ECOLUX-------
  "E01-ECOLUX":"Problème Switch door",
  "E02-ECOLUX":"Problème de rempissage en eau  : \n - vérifier l'installation \n - Vérifier la chambre d'air \n - Remplcaer l'électrovanne \n - Remplacer la carte",
  "E03-ECOLUX":"Problème de vidange : \n - Vérifier le tuyau de drainage \n - Vérifier la pompe \n - Remplcaer la carte",
  "E04-ECOLUX":"Problème excés de remplissage en eau : \n - Vérifier l'électrovanne \n - Vérifier l'électrovanne \n - Vérifier le pressostat \n - Remplacer la carte",
  "E05-ECOLUX":"Problème dans le circuit de chauffage : \n - Vérifier le thermistor \n -Vérifier la résistance \n - Vérifier la résistance",
  "E06-ECOLUX":"Problème moteur : \n - Vérifier le tachymétre \n - Vérifier les connexions du moteur \n - Remplacer le moteur \n - Remplacer la carte",
  "E08-ECOLUX":"Problème moteur : \n - Triac du moteur au niveau de la carte est en court-circuit \n - Réparer la carte \n - Remplacer la carte",
  "E10-ECOLUX":"Problème niveau d'eau: \n - Vérifier la chambre d'air et la durite de pressostat \n - Remplacer le pressostat \n - Remplacer la carte",
  /// -------Midea-------
  "E1-Midea":"Prblème arrivé d'eau : \n - Vérifier l'installation \n - Vérifier l'électrovanne \n - Vérifier le pressostat \n - Remplacer la carte",
  "E2-Midea":"Porte ouverte pendant l'essorage : \n - Vérifier l'installation \n - Vérifier la charge \n - Remplacer la carte",
  "E3-Midea":"Machine non équilibrée : \n - Vérifier l'installation \n - Vérifier la charge \n - Remplacer la carte",
  "E4-Midea":"Problème de vidange : \n - Vérifier le tuyau de drainage \n Vérifier la pompe \n Rmplacer la carte",
  "E5-Midea":"Probléme exés de remplissage en eau : \n - Vérifier l'électrovanne \n - Vérifier le pressostat \n - Remplacer la carte",
};*/
