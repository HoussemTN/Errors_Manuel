import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'libraries/Private.dart' as private;
import 'libraries/globals.dart' as globals;
import 'ComboBoxProductBrand.dart';
import 'model/Error.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:connectivity/connectivity.dart';


class AddError extends StatefulWidget {
  @override
  _AddErrorState createState() => _AddErrorState();
}
final _addFormKey = GlobalKey<FormState>();
final _addCodeController = TextEditingController();
final _addDescriptionController = TextEditingController();


class _AddErrorState extends State<AddError> {

  static _reviver(key, value) {
    /// last node is null , our value is map , the key of map starts with "-"
    if(key!=null && value is Map && key.contains("-")){
      return new Error.fromJson(value);
    }
    return value;
  }
  var jsonCodec = JsonCodec(reviver:_reviver);
  /// --- [FUNCTION] Sync Codes --- ///
  void syncRecords() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    var url=private.url+"codes.json";
    final httpClient = new Client();
    var response = await httpClient.read(url);
    /// Result nested map
    Map<String,dynamic> result=json.decode(response);
    /// Convert map values to list of map (code:"",Description:"")
    List finalResult=result.values.toList();
    for(int i=0;i<finalResult.length;i++){
      await prefs.setString(finalResult[i]["Code"],finalResult[i]["Description"]);
    }
  }
  /// ---[FUNCTION] Add Codes --- ///
  Future<String>_addRecord(Error error) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var connectivityResult = await (Connectivity().checkConnectivity());
    String _response="Resultat inconnu";
    /// If connected
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
      /// Sync Codes (avoid adding same codes)
       syncRecords();
      /// check if code exists
      if (prefs.containsKey(error.code)==false) {
        var json = jsonCodec.encode(
            {"Code": error.code, "Description": error.description});
        var url = private.url + "codes.json";
        final httpClient = new Client();
        var response = await httpClient.post(url, body: json);
        ///Code Added Successfully
        if (response.statusCode == 200) {
          _response = "Code ajouté avec succés";
          /// Clear Values
          _addDescriptionController.clear();
          _addCodeController.clear();
          syncRecords();
        }
        /// Code not inserted
        else {
          _response = "Code non ajouté";
        }
      }
      /// Code found in cache
      else {
        _response = "Code existe déjà";
      }
    }
    ///not Connected
    else{
     _response= "Veuillez vous connecter à l'internet";
    }
    return _response;
  }
  /// [FUNCTION] --- Show SnackBar--- ///
   void _showToast(BuildContext context,String _message){
    final scaffold = Scaffold.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text('$_message'),
        action: SnackBarAction(
            label: 'Ok', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
          appBar: AppBar(
              title: Text("Ajouter un code"),
          ),
      body: ListView(
        children: <Widget>[
     Form(
      key: _addFormKey,
          //Root Container
         child: Column(
           children: <Widget>[
             // Page Padding
             Padding(
               padding: EdgeInsets.all(8.0),
             ),
             Row(
               children: <Widget>[
                 SizedBox(width: 10.0,),
                 Expanded(
                     child: Column(
                       children: <Widget>[
                         Center(
                           child: Container(
                             child: TextFormField(
                               controller: _addCodeController,
                               decoration: InputDecoration(
                                 border: OutlineInputBorder(),
                                 labelText: "Code",
                                 hintText: "Code",
                               ),
                               validator: (value) {
                                 if (value.isEmpty) {
                                   return 'Code Obligatoire';
                                 }
                                 return null;
                               },
                             ),
                           ),
                         ),
                       ],
                     ),
                 ),

                 Flexible(
                     child: ComboBoxProductBrand()),
               ],
             ),
             Padding(
               padding: EdgeInsets.all(4.0),
             ),

             Container(
               padding: EdgeInsets.all(8.0),
               child: TextFormField(
                 keyboardType: TextInputType.multiline,
                 maxLines: 10,
                 maxLength: 1000,
                 controller: _addDescriptionController,
                 decoration: InputDecoration(
                   border: OutlineInputBorder(),
                   hintText: "Description",
                 ),
                 validator: (value) {
                   if (value.isEmpty) {
                     return 'Description Obligatoire';
                   }
                   return null;
                 },
               ),
             ),
             Padding(
               padding: EdgeInsets.all(4.0),
             ),
             Container(
               height: 50,
               width: MediaQuery.of(context).size.width*0.6,
               child:Builder(
                   builder: (context) => RaisedButton.icon(
                   color: Colors.blue,
                   label: Text('Ajouter',style: TextStyle(color: Colors.white,fontSize: 20),),

                   icon: Icon(
                     Icons.add, color: Colors.white,size: 25,),
                   onPressed: () {
                        /// Validate that description and code not empty
                       if (_addFormKey.currentState.validate() == true) {
                     String _codeImploded = _addCodeController.text + '-' + globals.currentBrand;
                     /// AddRecord returns[String response] then show response using snackBar
                     _addRecord(new Error(_codeImploded,_addDescriptionController.text)).then((response) {
                       _showToast(context, response);
                     });
                   }
                   }
                   ),),
             ),
           ],
         ),
        ),
        ],
      ),
    );
  }
}
